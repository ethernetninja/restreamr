#!/bin/bash
# restreamr manager script

##
# Color  Variables
##
green='\e[32m'
blue='\e[34m'
clear='\e[0m'

##
# Color Functions
##

ColorGreen(){
	echo -ne $green$1$clear
}
ColorBlue(){
	echo -ne $blue$1$clear
}

function update_keys() {
read -e -p "Enter YouTube Stream Key: " YT_KEY
read -e -p "Enter Facebook Stream Key: " FB_KEY
read -e -p "Enter Instagram Stream Key: " IN_KEY
read -e -p "Enter Twitter Stream Key: " TW_KEY
echo "Enter RTMP hostname:port/path for Twitter"
echo "Do not include rtmp:// or stream key in your entry. No trailing /"
read -e -p "Enter Twitter URL: " TW_URL
echo "********************"
echo "echo $YT_KEY"
echo "echo $FB_KEY"
echo "echo $IN_KEY"
echo "echo $TW_KEY"
echo "echo $TW_URL"
read -e -p "Verify the above information is correct. Press enter to continue..."

# Update NGINX
cat <<EOF > /etc/nginx/nginx.conf
load_module /usr/lib/nginx/modules/ngx_rtmp_module.so;
events {}
rtmp {
     server {
             listen 1935;
             chunk_size 4096; 
             application live {
                        live on;
                        record off;
                        # YouTube
                        push rtmp://a.rtmp.youtube.com/live2/$YT_KEY;
                        # Facebook
                        push rtmp://127.0.0.1:1936/rtmp/$FB_KEY;
                        # Instagram
                        push rtmp://127.0.0.1:1937/rtmp/$IN_KEY;
                        # Twitter
                        push rtmp://$TW_URL/$TW_KEY;
             }
             }
}
EOF
echo -ne $(ColorGreen 'Keys updated.')
echo ""
echo -ne $(ColorGreen 'You must restart/reload restreamr for key changes to take effect.')
echo ""
}

function reload_restreamr() {
    echo "Reloading restreamr"
    systemctl reload nginx
    sleep 1
    echo -ne $(ColorGreen 'Done.')
    echo ""
}

function restart_restreamr() {
    echo "Restarting restreamr"
    systemctl restart nginx
    sleep 1
    echo -ne $(ColorGreen 'Done.')
    echo ""
}

function restart_stunnel4() {
    echo "Restarting stunnel4"
    systemctl restart stunnel4
    echo -ne $(ColorGreen 'Done.')
    echo ""
}

function stop_restreamr() {
    echo "Stopping"
    systemctl stop nginx
    systemctl stop stunnel4
    echo -ne $(ColorGreen 'Done.')
    echo ""
}

function start_restreamr() {
    echo "Starting"
    systemctl start stunnel4
    sleep 1
    systemctl start nginx
    echo -ne $(ColorGreen 'Done.')
    echo ""
}

function status() {
    echo "restreamr Status"
    echo "nginx: " 
    systemctl is-active nginx
    echo "stunnel4: " 
    systemctl is-active stunnel4
    echo ""
    echo "Stop/Start/Restart services through the menu."
}

menu(){
echo -ne "
Restreamr Management Menu
$(ColorGreen '1)') Update Stream Keys (FB, Instagram, YT, Twitter)
$(ColorGreen '2)') Reload restreamer (soft restart)
$(ColorGreen '3)') Restart restreamr (hard restart)
$(ColorGreen '4)') Restart stunnel4 (tunnel to instagram/facebook)
$(ColorGreen '5)') Stop Stream (nginx/stunnel4)
$(ColorGreen '6)') Start Stream (nginx/stunnel4)
$(ColorGreen '7)') Status
$(ColorGreen '0)') Exit
$(ColorBlue 'Choose an option:') "
        read a
        case $a in
	        1) update_keys ; menu ;;
	        2) reload_restreamr ; menu ;;
	        3) restart_restreamr ; menu ;;
	        4) restart_stunnel4 ; menu ;;
	        5) stop_restreamr ; menu ;;
            6) start_restreamr ; menu ;;
            7) status ; menu ;;
		0) exit 0 ;;
		*) echo -e $red"invalid option."$clear; WrongCommand;;
        esac
}

# Call the menu function
menu
# restreamr

RTMP Restreamer tool via Terraform on AWS

Ingest 1 RTMP feed via NGINX and distribute to social media platforms (Facebook, Twitter, Youtube, Instagram)

### Getting started
- Create an S3 bucket for Terraform state storage
- Generate a key pair for EC2 instances
- Authenticate with AWS via awscli or set environment variables for Terraform

### Prepare Terraform
 - Provide bucket name for state storage in main.tf
 - Update key name in main.tf and define private key location in variables.tf

### Deploy restreamr
`terraform init`

`terraform plan`

`terraform apply`

### Manage restreamr

SSH to restreamr instance and use 'mgr' (/sbin/mgr) as root to manage restreamr

1. Update stream keys for social media platforms
2. Start restreamr

NGINX must be restarted when editing any stream keys.

## TODO
* Configure terraform plans with variables
* Define terraform outputs

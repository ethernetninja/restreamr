# restreamr aws
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  # Remote State Store
  backend "s3" {
    bucket = "pr9aa2ok2vpy2c"
    key    = "~/.aws/credentials"
    region = "us-east-1"
  }
  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

# VPC
resource "aws_vpc" "restreamr-vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  instance_tenancy     = "default"
  tags = {
    Name = "restreamr-vpc"
  }
}

# Subnet
resource "aws_subnet" "restreamr-vpc-subnet-1" {
  vpc_id                  = aws_vpc.restreamr-vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-1a"
  tags = {
    Name = "restreamr-vpc-subnet-1"
  }
}

# VPC IGW
resource "aws_internet_gateway" "prod-igw" {
  vpc_id = aws_vpc.restreamr-vpc.id
  tags = {
    Name = "restreamr-igw"
  }
}

# Routing
resource "aws_route_table" "restreamr-rt-1" {
  vpc_id = aws_vpc.restreamr-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.prod-igw.id
  }
  tags = {
    Name = "restreamr-rt-1"
  }
}

resource "aws_route_table_association" "restreamr-vpc-subnet-1" {
  subnet_id      = aws_subnet.restreamr-vpc-subnet-1.id
  route_table_id = aws_route_table.restreamr-rt-1.id
}

# Security
resource "aws_security_group" "restreamr-prod-fw" {
  vpc_id = aws_vpc.restreamr-vpc.id
  # Outbound
  # Allow ALL
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Inbound
  # Allow ALL
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# SSH Key
resource "aws_key_pair" "restreamr-key" {
  key_name   = "restreamr-key"
  public_key = file(var.PUBLIC_KEY_PATH)
}

# restreamr node 1
resource "aws_instance" "restreamr_node1" {
  ami           = "ami-04505e74c0741db8d"
  instance_type = "t2.xlarge"
  tags = {
    Name = "restreamr-node-1"
  }
  subnet_id              = aws_subnet.restreamr-vpc-subnet-1.id
  vpc_security_group_ids = ["${aws_security_group.restreamr-prod-fw.id}"]
  key_name               = aws_key_pair.restreamr-key.id

  # Provisioner
  provisioner "file" {
    source      = "restreamr_deploy.sh"
    destination = "/tmp/restreamr_deploy.sh"
  }
  provisioner "file" {
    source      = "conf/nginx.conf"
    destination = "/tmp/nginx.conf"
  }
  provisioner "file" {
    source      = "conf/stunnel.conf"
    destination = "/tmp/stunnel.conf"
  }
  provisioner "file" {
    source      = "conf/mgr.sh"
    destination = "/tmp/mgr.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname restreamr-node1.local",
      "chmod +x /tmp/restreamr_deploy.sh",
      "sudo /tmp/restreamr_deploy.sh",
      "sudo mv /tmp/nginx.conf /etc/nginx/nginx.conf",
      "sudo mv /tmp/stunnel.conf /etc/stunnel/stunnel.conf",
      "sudo mv /tmp/mgr.sh /sbin/mgr",
      "sudo chmod +x /sbin/mgr"
    ]
  }
  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = file("${var.PRIVATE_KEY_PATH}")
  }
}
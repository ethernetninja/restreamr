# Variables
variable "PRIVATE_KEY_PATH" {
  default = "~/.aws/aws-restreamr-key"
}
variable "PUBLIC_KEY_PATH" {
  default = "~/.aws/aws-restreamr-key.pub"
}
variable "EC2_USER" {
  default = "restreamr"
}

# More to add
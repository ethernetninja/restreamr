#!/bin/bash
# restreamr deployment

# Wait for cloud init
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
done

# Packages
apt-get update
apt-get -y install nginx libnginx-mod-rtmp vim dstat ffmpeg stunnel4 net-tools

# Start
echo "ENABLED=1 >> /etc/default/stunnel4"
systemctl enable nginx
systemctl enable stunnel4.service